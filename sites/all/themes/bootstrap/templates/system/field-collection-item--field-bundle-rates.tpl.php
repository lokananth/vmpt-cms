
<!---national bundles-->
<?php if(arg(1)== 38 || arg(1)== 207  || arg(1)== 323) { ?>


<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
<ul class="list-inline">
<?php if (!empty($content['field_bundle_price'])): ?>
<li class="first">
	  <div class="content"<?php print $content_attributes; ?>>
	   <h2>
		<?php
		  print render($content['field_bundle_price']);
		?>
		 </h2>
	  </div>


  <div class="content"<?php print $content_attributes; ?>>
  <?php if (!empty($content['field_vectone_customer'])): ?>
	<span>To buy, dial</span>
    <?php
      print render($content['field_vectone_customer']);
    ?>

	<?php endif; ?>
  </div>
  </li>
 <?php endif; ?> 
 
 <div class="right-section">
 
		 <?php if (!empty($content['field_special_price'])): ?>
		  <li>
			  <div class="content"<?php print $content_attributes; ?>>
				<?php 
				  print render($content['field_special_price']);
				?>
			  </div>
		   </li>
		  <?php endif; ?>
		  
		   <?php if (!empty($content['field_minutes_texts'])): ?>
			  <li>
				  <div class="content"<?php print $content_attributes; ?>>
					 <strong>
					<?php
					  print render($content['field_minutes_texts']);
					?>
					 </strong>
					<span>Minutes / Texts (Mix & Match)</span>
				  </div>
			   </li>
		   <?php endif; ?>
		   
		  <?php if (!empty($content['field_minutes'])): ?>
			  <li>
				  <div class="content"<?php print $content_attributes; ?>>
					 <strong>
					<?php
					  print render($content['field_minutes']);
					?>
					 </strong>
					<span>Minutes</span>
				  </div>
			   </li>
		   <?php endif; ?>
		   
		 <?php if (!empty($content['field_sms'])): ?>
		  <li>
			  <div class="content"<?php print $content_attributes; ?>>
				<strong>
				<?php
				  print render($content['field_sms']);
				?>
				 </strong>
				<span>Texts</span>
			  </div>
		  </li>
		 <?php endif; ?>
		 
		 <?php if (!empty($content['field_data'])): ?>
		  <li>
			  <div class="content"<?php print $content_attributes; ?>>
				<strong>
				<?php
				  print render($content['field_data']);
				?>
				</strong>
				<span>Data</span>
			  </div>
		  </li>
		  <?php endif; ?>
		  
		   <?php if (!empty($content['field_validity'])): ?>
		  <li>
			  <div class="content"<?php print $content_attributes; ?>>
				<strong>
				<?php
				  print render($content['field_validity']);
				?>
				</strong>
				<span>Validity</span>
			  </div>
		  </li>
		  <?php endif; ?>
		  
		  
		  		  
		<?php if (!empty($content['field_unlimited_calls'])): ?>
		  <li>
		  <div class="content"<?php print $content_attributes; ?>>

			 <strong>
			<?php
			  print render($content['field_unlimited_calls']);
			?>
			 </strong>
			
		  </div>
		  </li>
		  <?php endif; ?>
		  
		 <?php if (!empty($content['field_best_seller'])): ?>
		  <li> 
		  <div class="content"<?php print $content_attributes; ?>>

			<?php
			  print render($content['field_best_seller']);
			?>
		  </div>
		 </li>
		 <?php endif; ?>
		 
		 <?php if (!empty($content['field_online_customer_code'])): ?>
		  <li>
			  <div class="content"<?php print $content_attributes; ?>>
				<?php
				  print render($content['field_online_customer_code']);
				?>
			  </div>
		  </li>
		  <?php endif; ?>
		  
		</div>
  
  </ul>
</div>


<?php } 


//data bundles

 if(arg(1)== 39 || arg(1)== 322  || arg(1)== 118) { ?>

<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="top-data-bundle-section">
   <div class="content bundle-price"<?php print $content_attributes; ?>>
		<?php
		  print render($content['field_bundle_price']);
		?>
  </div>
  
  <div class="content validity"<?php print $content_attributes; ?>>
	  <?php if (!empty($content['field_validity'])): ?>
		<?php
		  print render($content['field_validity']);
		?>
		<?php endif; ?>
  </div>
  
   <div class="content"<?php print $content_attributes; ?>>
		<?php if (!empty($content['field_vectone_customer'])): ?>
		<span>To buy, dial</span>
		<?php
		  print render($content['field_vectone_customer']);
		?>
		<?php endif; ?>
  </div>
 </div>
   
  <div class="body-data-bundle-section">
  <div class="content special-price"<?php print $content_attributes; ?>>
   <?php if (!empty($content['field_special_price'])): ?>
    <?php 
      print render($content['field_special_price']);
    ?>
	<?php endif; ?>
	
  </div>
  <div class="content minutes-field"<?php print $content_attributes; ?>>
   <?php if (!empty($content['field_minutes'])): ?>
    <?php
      print render($content['field_minutes']);
    ?>
	<span>Minutes</span>
	<?php endif; ?>
  </div>
  
  <div class="content sms-field"<?php print $content_attributes; ?>>
  <?php if (!empty($content['field_sms'])): ?>
    <?php
      print render($content['field_sms']);
    ?>
	<span>Texts</span>
	<?php endif; ?>
  </div>
  
  <div class="content data-field"<?php print $content_attributes; ?>>
  <?php if (!empty($content['field_data'])): ?>
    <?php
      print render($content['field_data']);
    ?>
	<span>Data</span>
	<?php endif; ?>
  </div>
	
  <div class="content unlimited-calls"<?php print $content_attributes; ?>>
  <?php if (!empty($content['field_unlimited_calls'])): ?>
    <?php
      print render($content['field_unlimited_calls']);
    ?>
	<?php endif; ?>
  </div>
  
  <div class="content best-seller"<?php print $content_attributes; ?>>
  <?php if (!empty($content['field_best_seller'])): ?>
    <?php
      print render($content['field_best_seller']);
    ?>
	<?php endif; ?>
  </div>
  </div>
  
  <div class="bottom-data-bundle-section">
  


  <div class="content customer-code"<?php print $content_attributes; ?>>
  <?php if (!empty($content['field_online_customer_code'])): ?>
    <?php
      print render($content['field_online_customer_code']);
    ?>
	<?php endif; ?>
  </div>
 </div>
</div>

<?php } 

// homepage

if (drupal_is_front_page()) {?>

<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="top-bundle-section">
  <?php if (!empty($content['field_bundle_price'])): ?>
  <div class="content bundle-price"<?php print $content_attributes; ?>>
		<?php
		  print render($content['field_bundle_price']);
		?>
  </div>
  <?php endif; ?>
  </div>
   
  <div class="body-bundle-section">
  
  <?php if (!empty($content['field_special_price'])): ?>
	  <div class="content special-price"<?php print $content_attributes; ?>>
		<?php 
		  print render($content['field_special_price']);
		?>
	  </div>
  <?php endif; ?>
  
 <?php if (!empty($content['field_minutes_texts'])): ?>
	<div class="content minutes-field"<?php print $content_attributes; ?>>
		<?php
			print render($content['field_minutes_texts']);
		?>
			<span>Minutes / Texts (Mix & Match)</span>
	</div>
  <?php endif; ?>
  
<?php if (!empty($content['field_minutes'])): ?>	
  <div class="content minutes-field"<?php print $content_attributes; ?>>

    <?php
      print render($content['field_minutes']);
    ?>
	<span>Minutes</span>
  </div>
  <?php endif; ?>
  
<?php if (!empty($content['field_sms'])): ?>
  <div class="content sms-field"<?php print $content_attributes; ?>>

    <?php
      print render($content['field_sms']);
    ?>
	<span>Texts</span>

  </div>
 <?php endif; ?>
 
  <?php if (!empty($content['field_data'])): ?>	
  <div class="content data-field"<?php print $content_attributes; ?>>
    <?php
      print render($content['field_data']);
    ?>
	<span>Data</span>
  </div>
<?php endif; ?>

<?php if (!empty($content['field_unlimited_calls'])): ?>
  <div class="content unlimited-calls"<?php print $content_attributes; ?>>
    <?php
      print render($content['field_unlimited_calls']);
    ?>
  </div>
<?php endif; ?>

<?php if (!empty($content['field_best_seller'])): ?>
  <div class="content best-seller"<?php print $content_attributes; ?>>
    <?php
      print render($content['field_best_seller']);
    ?>
  </div>
<?php endif; ?> 

<?php if (!empty($content['field_validity'])): ?>  
  <div class="content validity"<?php print $content_attributes; ?>>
    <?php
      print render($content['field_validity']);
    ?>
	<span>Validity</span>
  </div> 
 <?php endif; ?> 
 </div>
  
 <div class="bottom-bundle-section">
  
 <?php if (!empty($content['field_vectone_customer'])): ?>
  <div class="content"<?php print $content_attributes; ?>>
    <div class="vectone-customer">
	<span>To buy, dial</span>
    <?php
      print render($content['field_vectone_customer']);
    ?>
	</div>
  </div>
<?php endif; ?>

<?php if (!empty($content['field_online_customer_code'])): ?>	
  <div class="content customer-code"<?php print $content_attributes; ?>>
    <?php
      print render($content['field_online_customer_code']);
    ?>
  </div>
<?php endif; ?>
 </div>
</div>

<?php }?>