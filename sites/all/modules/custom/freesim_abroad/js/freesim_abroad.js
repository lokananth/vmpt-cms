(function ($) {
	$(document).ready(function(){

		$("#freesim_btn_submit").removeAttr("disabled");

		$("#freesim-registration-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$(this).removeClass('error');
		}); 
		
		$("#edit-billing-address-chkbox").on('click', function(){
			if($("#edit-billing-address-chkbox").is(':checked')){
				$.ajax({
					url: Drupal.settings.basePath + 'freesim/abroad/getdeliveryaddress',
					type: "POST",
					dataType: "json",
					success: function(data) {
						//console.log(data);
						$("#address1").val( data.address1 );
						$("#address2").val( data.address2 );
						$("#town").val( data.town );
						$("#pin_code").val( data.postcode );
					}
				});
			}else{
				$("#address1").val('');
				$("#address2").val('');
				$("#town").val('');
				$("#pin_code").val('');
			}
		});
		
		$(".dst_country_list").on('change', function(){
			var dst_country = $(this).val();
			$(".span_sim_cost").text('');
			$(".span_delivery_cost").text('');
			$('input[name="delivery_cost"]').val('');
			if(dst_country != ''){
				$.ajax({
					url: Drupal.settings.basePath + 'freesim/abroad/getdeliverycost',
					type: "POST",
					data:{
						dst_country: dst_country
					},
					dataType: "json",
					success: function(data) {
						console.log(data);
						if(data[0].errcode == 0){
							$(".span_sim_cost").text('Free');
							$(".span_delivery_cost").text(data[0].Currency_Code + ' ' + data[0].Delivery_Cost );
							$('input[name="delivery_cost"]').val( data[0].Delivery_Cost );
						}
					}
				});
			}
		});

	});
})(jQuery);