(function ($) {
	$(document).ready(function(){

		//VM Features - Mobile Operators
		$( "#dropdown_dst_country_list" ).change(function(){

				var ccode = $(this).val();
				$('#div-operators-list').remove();
				$.ajax({
					url: Drupal.settings.basePath+"vmfeatures/operators/result",
					type: "POST",
					data: {
						country_code: ccode
					},
					success: function(data){
						if(data) { 
							$('.dst_country_list').append('<div id="div-operators-list">'+data+'</div>');
						} 
					}
				});
		});			
		
		//VM Features - Mobile Internet : Upon selection of Manufacturer, need to reset the Duscursion Markup.
		$( "#edit-dropdown-handset-manufacturer" ).change(function(){
			$('div[id^="edit-mob-discursion"]').html("");
		});
		
	});
})(jQuery);