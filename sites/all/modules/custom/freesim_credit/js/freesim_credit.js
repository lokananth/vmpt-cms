(function ($) {
	$(document).ready(function(){

		$("#freesim_btn_submit").removeAttr("disabled");

		$("#freesim-registration-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$(this).removeClass('error');
		}); 

		$("#edit-billing-address-chkbox").on('click', function(){
			if($("#edit-billing-address-chkbox").is(':checked')){
				$.ajax({
					url: Drupal.settings.basePath + 'freesim/credit/getdeliveryaddress',
					type: "POST",
					dataType: "json",
					success: function(data) {
						//console.log(data);
						$("#address1").val( data.address1 );
						$("#address2").val( data.address2 );
						$("#town").val( data.town );
						$("#pin_code").val( data.postcode );
					}
				});
			}else{
				$("#address1").val('');
				$("#address2").val('');
				$("#town").val('');
				$("#pin_code").val('');
			}
		});

	});
})(jQuery);