<?php

function paysafeApiPost($apiUrl, $data = Array(), $isAuth = true) {

	$header[0] = 'Authorization: mundiovectone y7QxCUMIxl3S3wpefjksjJbk9PS9g+kqrHMMbfOGWtQ=';
	$header[1] = 'Mundio-Api-PublicKey: UGF5U2FmZQ==';
	$header[2] = 'Content-MD5: PaySafe';
	$header[3] = 'User-Agent: mundiovectone';
	$header[4] = 'Accept: application/json';
	$header[5] = 'Accept-Charset: UTF-8';
	$header[6] = 'Content-Type: application/json';
	$header[7] = 'Host: 192.168.2.102:1123';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
	if ($data) {
		$str = json_encode($data);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
	}
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	
	//echo "<pre>";
	//print_r($header);
	//exit;
    $output = curl_exec($ch);
	curl_close($ch);
    return json_decode($output, true);
}
