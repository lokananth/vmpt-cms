(function($) {
    $(document).ready(function() {
		
        $("#vcode").on('click', function(e) {
            $.ajax({
                url: Drupal.settings.basePath + 'myaccount/register/pin_resend',
                type: "POST",
                success: function(data) {
                    window.location = data;
                }
            });
        });

        //BEGIN: Register Step 1 - form validation
		$("#myaccount-registration-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});

		//END: Register Step 1 - form validation		
		
		//Login Form Validation
		$("#myaccount-login-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});
		
        // Forgot password form validation
		$("#myaccount-forgot-password-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});        
		
		// Reset password form validation
		$("#myaccount-reset-password-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$("#replace_validation_div").html('');
		});		

    });
 
})(jQuery);